import React from 'react'
import Item from './ExpenseItem'
import { MdDelete } from 'react-icons/md'

const ExpenseList = ({ expenses, handleEditItem, handleDeleteItem, handleClearItems }) => {
    return (
        <>
            <ul className="list">
                {expenses.map((expense) => {
                    return <Item key={expense.id} expense={expense} handleDeleteItem={handleDeleteItem} handleEditItem={handleEditItem} />;
                })}
            </ul>
            {expenses.length > 0 && (
                <button className="btn" onClick={handleClearItems}>Clear expenses
                    <MdDelete className="btn-icon" />
                </button>
            )}
        </>
    );
};

export default ExpenseList
