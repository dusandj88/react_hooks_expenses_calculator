import React, { useState, useEffect } from 'react';
import './App.css';
import ExpenseList from './components/ExpenseList'
import Alert from "./components/Alert";
import ExpenseForm from "./components/ExpenseForm";
import uuid from 'uuid/v4';

const initialExpenses = localStorage.getItem('expenses') ? JSON.parse(localStorage.getItem('expenses')) : [];

function App() {
  const [expenses, setExpenses] = useState(initialExpenses);
  const [charge, setCharge] = useState('');
  const [amount, setAmount] = useState('');
  const [alert, setAlert] = useState({ show: false });
  const [edit, setEdit] = useState(false);
  const [id, setId] = useState(0);

  useEffect(() => {
    localStorage.setItem('expenses', JSON.stringify(expenses))
  }, [expenses]);

  const handleCharge = e => {
    setCharge(e.target.value);
  };

  const handleAmount = e => {
    setAmount(e.target.value);
  };

  const handleAlert = ({ type, text }) => {
    setAlert({ show: true, type, text });
    setTimeout(() => {
      setAlert({ show: false })
    }, 3000);
  }

  const handleSubmit = e => {
    e.preventDefault();
    if (charge !== "" && amount > 0) {
      if (edit) {
        let editedExpenses = expenses.map(expense => {
          return expense.id === id ? { ...expense, charge, amount } : expense
        });
        setExpenses(editedExpenses);
        setEdit(false);
      } else {
        const expense = {
          id: uuid(),
          charge,
          amount
        }
        setExpenses([...expenses, expense]);
        handleAlert({ type: 'success', text: 'item added' });
      }
      setCharge("");
      setAmount("");
    } else {
      handleAlert({ type: 'danger', text: 'Invalid item info' });
    }
  };

  const handleClearItems = () => {
    setExpenses([]);
    handleAlert({ type: 'danger', text: 'All items deleted' });
  };

  const handleDeleteItem = (id) => {
    let filteredExpenses = expenses.filter(item => item.id !== id);
    setExpenses(filteredExpenses);
    handleAlert({ type: 'danger', text: 'Item successfully deleted' });
  };

  const handleEditItem = (id) => {
    let expense = expenses.find(item => item.id === id);
    let { charge, amount } = expense;
    setCharge(charge);
    setAmount(amount);
    setEdit(true);
    setId(id);
  };

  return (
    <>
      {alert.show && <Alert type={alert.type} text={alert.text} />}
      <Alert />
      <h1>budget calculator</h1>
      <main className="App">
        <ExpenseForm charge={charge} amount={amount} handleAmount={handleAmount} handleCharge={handleCharge} handleSubmit={handleSubmit} edit={edit} />
        <ExpenseList expenses={expenses} handleDeleteItem={handleDeleteItem} handleEditItem={handleEditItem} handleClearItems={handleClearItems} />
      </main>
      <h1>
        Total Spending : {" "}
        <span className="total">
          ${expenses.reduce((totalExpenses, currentExpense) => {
            return (totalExpenses += parseInt(currentExpense.amount));
          }, 0)}
        </span>
      </h1>
    </>
  );
}

export default App;
